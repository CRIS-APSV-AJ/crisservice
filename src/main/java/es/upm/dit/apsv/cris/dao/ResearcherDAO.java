package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Researcher;

public interface ResearcherDAO {
	Researcher create( Researcher researcher );
	Researcher read( String researcherId );
	Researcher update( Researcher researcher );
	Researcher delete( Researcher researcher );

	List<Researcher> readAll();
	Researcher readByEmail(String email);
}
